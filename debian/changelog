csh (20240808-2) unstable; urgency=medium

  * Fix inconsistent declaration that breaks 32-bit builds (g++-14 patch)
    Closes: #1078737

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 15 Aug 2024 09:12:42 +0100

csh (20240808-1) unstable; urgency=medium

  * New git snapshot. Includes timespec changes needed for clean g++14
    build
  * g++-14.patch: Fix FTBFS with g++-14. Closes: #1074892

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 08 Aug 2024 12:02:13 +0100

csh (20230828-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Install programs into /usr/bin. (Closes: #1073599)
  * Fix urgency of changelog entry 20110502-7.

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 08 Jul 2024 01:27:48 +0200

csh (20230828-1) unstable; urgency=medium

  * New snapshot from upstream
    - Closes: #933057
  * Take maintainesrhip. Closes: #1049952
  * Patches updated:
    - 12_glibc_union_wait.diff now upstream
    - pointer_deref_comparison.patch merged upstream
  * Add d/gpb.conf file
  * Point Vcs-Git to DEP-14 branch names
  * Standards-Version: 4.6.2
  * Fix for FTCBFS by Helmut Grohe: Closes: #945058
  * Build-dep on publib for xfree,etc.
  * Clean up to allow repeat builds. Closes: #1044860

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 28 Aug 2023 10:54:37 +0100

csh (20110502-7) unstable; urgency=medium

  [ Lukas Märdian ]
  * Fix FTBFS with glibc 2.32: sys_siglist[] and sys_sigabbrev[] have been
    deprecated in favor of strsignal()
    - Refresh d/p/09_sys_signame.diff
    - Add d/p/glibc-strsignal.diff
    Closes: #996642

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 26 Oct 2021 08:47:17 +0100

csh (20110502-6) unstable; urgency=medium

  * Patch from Etienne Mollier for FTBFS with gcc10. Closes: #957113
  * Standards-Version: 4.5.0
  * Debhelper level 13

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 23 Jul 2020 09:41:59 +0100

csh (20110502-5) unstable; urgency=medium

  * Fix for segfault on eval. Patch thanks to Keith Thompson, Graham Inggs.
    Closes: #933057
  * Standards-Version: 4.4.0
  * Add vcs and homepage to d/control
  * Use debhelper-compat (=12)

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 22 Sep 2019 09:10:45 +0100

csh (20110502-4) unstable; urgency=medium

  * Take over as maintainer. Closes: #868691
  * Standards-Version: 4.2.0

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 10 Sep 2018 12:49:15 +0100

csh (20110502-3.1) unstable; urgency=medium

  * Nob-maintainer upload
  [ Alastair McKinstry ]
  * Fix FTBFS by misuse of -Werror / #warning in <bsd/vis.h>. Closes: #906457
  * Mark csh as M-A: foreign. Closes: #899199.
  [ Gilles Filippini ]
  * Add patches/13_fix_arithmetic_precedence.diff to fix precedence for
    consecutive ADDOP (+, -) or MULOP (*, /, %) (closes: #862221)

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 03 Sep 2018 17:21:24 +0100

csh (20110502-3) unstable; urgency=medium

  * QA upload.
  * Set maintainer to the QA group.
  * Bump dh to 10.
  * Fix FTBFS with gcc-7 (Closes: #853361).
  * -Wno-error=unused-result, to work around gcc brain damage
    (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66425)

 -- Adam Borowski <kilobyte@angband.pl>  Mon, 28 Aug 2017 11:46:44 +0200

csh (20110502-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patches/12_glibc_union_wait.diff to remove usage of deprecated BSD
    union wait type. Fixes the FTBFS against glibc 2.24 (Closes: #833968).

 -- Aurelien Jarno <aurel32@debian.org>  Fri, 30 Sep 2016 18:25:06 +0200

csh (20110502-2.1) unstable; urgency=low

  * Non-maintainer upload.
  [ gregor herrmann ]
  * Fix "FTBFS: Makefile:24: *** missing separator.  Stop.":
    use bmake debhelper addon:
    - d/control: build-depend on bmake (>= 20130730-2)
    - d/rules: use bmake buildsystem, update override_dh_auto_{build,clean}
    (Closes: #718068)

 -- YunQiang Su <syq@debian.org>  Fri, 18 Jul 2014 09:28:44 +0800

csh (20110502-2) unstable; urgency=medium

  * 02_libbsd.diff: Use <bsd/vis.h> rather than <vis.h>.  Closes: #629684.
  * Drop "standard login shell on BSD systems" from the description.
    Thanks to Thorsten Glaser for the suggestion.

 -- Matej Vela <vela@debian.org>  Tue, 05 Jul 2011 14:27:00 +0200

csh (20110502-1) unstable; urgency=low

  * New upstream release.
    - Includes fix for exp2 conflict from #258588.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Switch to debhelper 8.
  * 01_gnu_source.diff: Remove now-unused `-D__BSD_VISIBLE' (leaving only
    `-D_GNU_SOURCE').
  * 02_libbsd.diff: Remove convenience copies of OpenBSD's strlcpy and vis;
    link to libbsd instead.
  * 08_glob.diff: Remove convenience copy of OpenBSD's glob; refactor
    to avoid any of the areas where BSD and GNU implementations differ
    (GLOB_MAGCHAR, GLOB_NOCHECK, GLOB_NOMAGIC, gl_matchc).
  * 11_groff_schar.diff: Make footnote marks legible in csh.txt.
  * debian/copyright:
    - Include full license text instead of referring to
      /usr/share/common-licenses/BSD.
    - Switch to machine-readable format (DEP-5).
  * debian/rules (get-orig-source):
    - Derive version from last-modified time rather than current date.
    - Clean up temporary directory in case of errors.
  * Conforms to Standards version 3.9.2.

 -- Matej Vela <vela@debian.org>  Fri, 13 May 2011 22:54:42 +0200

csh (20070713-2) unstable; urgency=medium

  * _glob.c (globextend): Use sysconf(_SC_ARG_MAX) rather than ARG_MAX.
    Closes: #518860.
  * Switch to debhelper 7.
  * debian/doc-base: Update section to Shells.
  * debian/postinst, debian/prerm: Use which rather than type.
  * Conforms to Standards version 3.8.0.

 -- Matej Vela <vela@debian.org>  Mon, 09 Mar 2009 09:15:35 +0100

csh (20070713-1) unstable; urgency=low

  * New upstream release.
  * debian/menu: Move to Applications/Shells.

 -- Matej Vela <vela@debian.org>  Fri, 13 Jul 2007 23:52:21 +0200

csh (20060813-1) unstable; urgency=low

  * New upstream release.
  * Switch to cdbs.
  * csh.c, csh.h, debian/preinst: Remove potato compatibility code.
  * debian/postinst: Invoke add-shell unconditionally, to handle the case
    where the package is re-installed after being removed.
  * Conforms to Standards version 3.7.2.

 -- Matej Vela <vela@debian.org>  Sun, 13 Aug 2006 15:53:42 +0200

csh (20060413-1) unstable; urgency=low

  * New upstream release.
  * Since glibc is broken with respect to GLOB_NOMAGIC and backslashes
    (and its maintainers refuse to see the light), switch back to BSD's
    implementation of glob.  Closes: #220170.
  * Incidentally, this removes the need to reimplement glob_t.gl_matchc,
    fixing a bug that was causing `No match' errors when some patterns did
    match and some didn't.  Closes: #350869.
  * Prefix BSD libc sources (glob.c, strlcpy.c, vis.c) with an underscore
    to avoid a conflict with our own glob.c.
  * _glob.c (globtilde): Replace issetugid with calls to get{e,}{u,g}id.
  * Use add-shell and remove-shell.  Closes: #361544.
  * Switch to debhelper 5.
  * debian/copyright: Document `debian/rules get-orig-source'.
  * debian/doc-base: Rephrase abstract.
  * debian/rules: Set CVS_RSH to ssh.
  * Conforms to Standards version 3.6.2.

 -- Matej Vela <vela@debian.org>  Thu, 13 Apr 2006 18:48:29 +0200

csh (20050313-1) unstable; urgency=low

  * New upstream release.
  * Revert to a flat source tree, it does make things simpler.
  * exp.c (exp2): Rename to csh_exp2 to avoid conflict with gcc builtin
    (thanks to Andreas Jochens).  Closes: #258588.
  * glob.c (libglob): GLOB_NOMAGIC has been fixed in glibc 2.3 (#106097).
  * proc.c (dokill): glibc has an undocumented sys_sigabbrev array with
    signal names, so we no longer have to generate our own.
  * vis.c (isvisible): Comparison with UCHAR_MAX triggers `always true'
    warnings for unsigned char arguments; since this is already checked by
    isascii, remove it.  Closes: #268438.
  * vis.h: Remove OpenBSD-specific `bounded' attribute.
  * Switch to debhelper 4.
  * debian/doc-base: Add doc-base support.
  * debian/menu: Add menu file (thanks to Bill Allombert).  Closes: #187629.
  * debian/rules:
    - Re-enable -Werror on alpha now that #97603 is fixed.
    - Make sure groff uses traditional (non-ANSI) escape sequences.
    - Remove support for DEB_BUILD_OPTIONS=debug.
    - Add support for DEB_BUILD_OPTIONS=noopt.
  * Conforms to Standards version 3.6.1.

 -- Matej Vela <vela@debian.org>  Sun, 13 Mar 2005 22:14:05 +0100

csh (20020413-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Urgency=high for sarge-targetted RC bugfix.
  * Fix implicit signed char assumptions in source, addressing FTBFS bugs
    on arm/powerpc/s390 (closes: #268438).

 -- Steve Langasek <vorlon@debian.org>  Mon,  6 Sep 2004 19:31:44 -0700

csh (20020413-1) unstable; urgency=low

  * New upstream version.
  * Removed the /usr/bin/csh compatibility symlink.  Since /usr/bin
    precedes /bin in the PATH, a configure script would use the former
    location instead of the one managed by alternatives, and things
    wouldn't work with tcsh (cf. #106547).  Obviously, this doesn't break
    anything that isn't already broken for tcsh.

 -- Matej Vela <vela@debian.org>  Sat, 13 Apr 2002 18:34:51 +0200

csh (20020213-1) unstable; urgency=low

  * New upstream version.  Handles out of range argv subscripts.
    Closes: #131693.

 -- Matej Vela <vela@debian.org>  Wed, 13 Feb 2002 23:03:47 +0100

csh (20011113-1) unstable; urgency=high

  * New upstream version.
  * csh.c: On glibc (<< 2.2), call cookie_seek_function_t with an
    off_t instead of an off64_t *.  The package now builds on potato.
    Closes: #119344.
  * csh.c: Leave standard streams open for progprintf.
  * csh.c, proc.c: Substitute fpurge with the recently added __fpurge.
  * csh.h: On the Hurd, <sys/param.h> seems to define MAXPATHLEN to
    PATH_MAX without defining the latter; provide a default.
  * func.c (doprintf): progprintf uses stdout/stderr, so flush those
    rather than cshout/csherr.
  * pathnames.h (_PATH_LOGIN): login is in /bin, not /usr/bin.

 -- Matej Vela <vela@debian.org>  Tue, 13 Nov 2001 20:53:00 +0100

csh (20010813-1) unstable; urgency=high

  * New upstream version.
  * debian/rules: stdio.h causes a cpp warning on alpha; conditionally
    disable -Werror until #97603 is fixed.  Closes: #106285.
  * glob.c: GLOB_NOMAGIC produces false positives for patterns like
    `*/xxx'; work around it until #106097 is fixed.
  * Conforms to Standards version 3.5.6.

 -- Matej Vela <vela@debian.org>  Mon, 13 Aug 2001 20:33:24 +0200

csh (20010613-1) unstable; urgency=low

  * New upstream version.
  * New maintainer.  Closes: #92493.
  * debian/prerm: `update-alternatives --remove' wants /bin/bsd-csh, not
    /bin/csh.
  * Conforms to Standards version 3.5.5.

 -- Matej Vela <vela@debian.org>  Wed, 13 Jun 2001 03:31:52 +0200

csh (20010413-1) unstable; urgency=medium

  * New upstream version, re-ported from OpenBSD-current:
    * csh.c: Include <time.h> for time_t etc.  Closes: #90859.
    * csh.c (main, readf, writef, seekf, closef): funopen substituted
      with fopencookie.
    * csh.c (pintr1), proc.c (pprint): fpurge substituted with fflush.
      (Anything better?)
    * csh.h: Define MAXPATHLEN to 4096 if it isn't already defined (e.g.
      on the Hurd).  If X11 and Tcl can get away with it, so can we. ;-)
      Closes: #54993.
    * glob.c (libglob):
      * Don't set GLOB_QUOTE, it's already default.
      * Work around gl_matchc, this time correctly.  Closes: #88780.
    * misc.c (closem): Upstream fixed it to use `sysconf (_SC_OPEN_MAX)'
      rather than NOFILE.  Closes: #63650.
    * proc.c (dokill), proc.h, siglist.in: Use our own sys_signame
      array, constructed from siglist.in (taken from pdksh 5.2.14).
  * Package is orphaned (see #92493); maintainer set to Debian QA Group.
  * Converted to debhelper.  Closes: #91435.
  * Conforms to Standards version 3.5.2:
    * Added build dependencies.  Closes: #90860.
    * debian/rules: Support the `debug' build option.
    * debian/copyright: Updated.
  * Changed priority to optional, catching up with the override file.
  * debian/lintian: Override the `binary-without-manpage: csh' Lintian
    error caused by the /usr/bin/csh -> /bin/csh symlink.
  * debian/rules: Added a `get-orig-source' target.
  * Removed pre-bo (<< 5.26-6) compatibility code.

 -- Matej Vela <vela@debian.org>  Fri, 13 Apr 2001 20:43:09 +0200

csh (5.26-12) unstable; urgency=low

  * move csh to /bin/bsd-csh

 -- Michael Stone <mstone@debian.org>  Tue,  2 Jan 2001 07:36:42 -0500

csh (5.26-11) unstable; urgency=low

  * New maintainer.
  * Updated policy version

 -- Michael Stone <mstone@debian.org>  Tue,  2 Jan 2001 06:59:11 -0500

csh (5.26-10) unstable; urgency=low

  * New maintainer.

 -- Edward Brocklesby <ejb@debian.org>  Sat, 10 Jul 1999 11:12:48 -0700

csh (5.26-9) unstable; urgency=low

  * libc6 release.
  * Several fixes (hacks really, for the most part) to get it to build on
    libc6.
  * Clean up const.h in debian/rules clean.
  * Run dpkg-shlibdeps.
  * Orphaned the package, now maintained by debian-qa.

 -- Joey Hess <joeyh@master.debian.org>  Thu, 20 Nov 1997 20:57:46 -0500

csh (5.26-8) stable unstable; urgency=HIGH

  * Added glob functions from 4.4BSD libc to work around missing BSD
    compatibility in Linux libc. That fixes Bug#6501.
  * Replaced manual page with newer one from NetBSD.  This fixes Bug#5872.
  * Changed Maintainer address to <dominik@debian.org>.
  * Added USD manual "An Introduction to the C shell" from NetBSD.
    This will be installed both as Latin-1 text and postscript file.

 -- Dominik Kubla <dominik@debian.org>  Sat, 25 Jan 1997 14:41:04 +0100

csh (5.26-7) unstable; urgency=low

  * Minor bugfixes in debian/rules.  Should now comply with "Debian Programmers
    Manual", section 3.2.1

  * manual page is now installed in 'gzip -9' format as per
    "Debian Policy Manual", section 3.2.1

  * changed priority from 'optional' to 'standard'.

 -- Dominik Kubla <Dominik.Kubla@Uni-Mainz.DE>  Mon, 30 Sep 1996 16:43:37 +0200

csh (5.26-6) unstable; urgency=low

  * Applied patches provided by Randy Gobbel <gobbel@cogsci.ucsd.edu> to
    fix a malloc and a string match bug.

  * copyright file is now installed in /usr/doc/csh/copyright as per
    "Debian Policy Manual", Section 3.2.6

  * Package is now compiled with CFLAGS set to '-O2 -g -Wall' as per
    "Debian Policy Manual", Section 4.1

  * csh does now use alternatives instead of diversions to coexist with
    tcsh.  Therefore it conflicts with tcsh <= 6.06-3 because tcsh does
    not yet use alternatives.

 -- Dominik Kubla <Dominik.Kubla@Uni-Mainz.DE>  Sat, 28 Sep 1996 19:48:15 +0200

csh (5.26-5) unstable; urgency=low

  * Converted to new packaing standards.

 -- Dominik Kubla <Dominik.Kubla@Uni-Mainz.DE>  Thu, 12 Sep 1996 09:44:45 +0200
